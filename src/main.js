import Vue from 'vue'
import VueResource from 'vue-resource';
import VueKonva from 'vue-konva';
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHandPointer, faPlusSquare, faPlayCircle, faPauseCircle } from '@fortawesome/free-regular-svg-icons'
import { faMousePointer, faCloudUploadAlt, faTrashAlt, faChevronCircleLeft, faChevronCircleRight, faSyncAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VTooltip from 'v-tooltip';

// Manage HTTP
Vue.use(VueResource);

// Manage font awesome icons
library.add(
  faMousePointer,
  faHandPointer,
  faPlusSquare,
  faPlayCircle,
  faPauseCircle,
  faCloudUploadAlt,
  faTrashAlt,
  faChevronCircleLeft,
  faChevronCircleRight,
  faSyncAlt);

Vue.component('font-awesome-icon', FontAwesomeIcon);

// Manage tooltips
Vue.use(VTooltip);

// Manage Canvas
Vue.use(VueKonva);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
